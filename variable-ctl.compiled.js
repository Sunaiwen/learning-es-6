// rest params
'use strict';

function testRestParams(name) {
  for (var _len = arguments.length, params = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    params[_key - 1] = arguments[_key];
  }

  console.log(params);
}

function main() {
  var name = arguments.length <= 0 || arguments[0] === undefined ? 'sunaiwen' : arguments[0];
  var gender = arguments.length <= 1 || arguments[1] === undefined ? name === 'sunaiwen' ? 'male' : 'female' : arguments[1];
  var age = arguments.length <= 2 || arguments[2] === undefined ? 18 : arguments[2];
  return (function () {
    testRestParams(name, gender, age);
    return 'Hello, everyone, I\'m ' + name + ', and I\'m a ' + gender + ', and I\'m ' + age;
  })();
}

var one = 1;
var two = 2;

console.log(one);
console.log(two);

var _gender = { gender: 'male' };
var _gender$name = _gender.name;
var name = _gender$name === undefined ? 'maozedong' : _gender$name;
var _gender$gender = _gender.gender;
var gender = _gender$gender === undefined ? 'female' : _gender$gender;

console.log(name);
console.log(gender);
