'use strict'

let a = 0
const ITERATOR_COUNT = 5

function test() {
  if(ITERATOR_COUNT > 0) {
    const ITERATOR_COUNT = 100
    console.log(ITERATOR_COUNT)
  }

  for(let a = 0; a < ITERATOR_COUNT; a++) {
    setTimeout(function() {
      console.log(a)
    }, a * 100)
  }
}

test()