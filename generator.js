function* gen() {
  for(var i = 0; i < 6; i++) {
    try {
      console.log(yield i)
    } catch(e) {
      console.log('error')
    }
  }
}

function handle() {
  var g = gen()
  console.log(g.next())
  console.log(g.next(100))
  g.throw(1000)
  g.next(9999)
  g.return(1000)
}

handle()