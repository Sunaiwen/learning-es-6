'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _module2Js = require('./module-2.js');

exports['default'] = {
  sayHello: function sayHello() {
    var name = arguments.length <= 0 || arguments[0] === undefined ? 'Obama' : arguments[0];

    return 'Hello, ' + (0, _module2Js.composeName)(name);
  }
};
module.exports = exports['default'];
