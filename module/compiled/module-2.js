'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.composeName = composeName;

function composeName(name) {
  return 'My name is ' + name;
}
