import {composeName} from './module-2.js'

export default {
  sayHello: function(name='Obama') {
    return 'Hello, ' + composeName(name)
  }
}