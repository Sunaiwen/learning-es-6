class Creature {
  constructor(name) {
    this.name = name
  };
  hello() {
    console.log(this.name)
  };
  get name() {
    return this._name
  };
  set name(val) {
    this._name = val
    this.watchName()
  };
  watchName() {
    console.log('watch: ' + this._name)
  }
}


class Dog extends Creature {
  constructor(name) {
    console.log('haha')
    super(name)
  };
  bark() {
    console.log('bark!!!')
  }
}

var dog = new Dog('Puppy')