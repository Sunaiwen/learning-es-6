// rest params
function testRestParams(name, ...params) {
  console.log(params)
}

// default function params
function main(name='sunaiwen', gender= (name ==='sunaiwen') ? 'male': 'female', age=18) {
  testRestParams(name, gender, age)
  return `Hello, everyone, I'm ${name}, and I'm a ${gender}, and I'm ${age}`
}

// destructuring
var [one, two] = [1, 2]
console.log(one)
console.log(two)

var {name='maozedong', gender='female'} = {gender: 'male'}
console.log(name)
console.log(gender)