var foo = new Map()
foo.set('name', 'sunaiwen')
foo.set('name', 'sunaiwen')

// use object as a Map key
var bar = {gender: 'P'}
foo.set(bar, 'P')
console.log(foo.get(bar))

var weakmap = new WeakMap()
weakmap.set({name:'haha'}, 'haha')
console.log(weakmap)