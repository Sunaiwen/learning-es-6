// The THIS inheritance
var foo = {
  bar: () => {
    var self = this
    ;(() => {
      console.log(self === this)
    })()
  }
}

foo.bar()

// doesn't have arguments object passing in
;((a,...b)=>{
  console.log(a)
  console.log(b)
})(1,2,3)

// shorthand for writing function in object
var bar = {
  test() {
    console.log(this.name === 'bar')
  },
  name: 'bar'
}
bar.test()