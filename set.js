var cookie = new Set()
// adding a same value twice has no effect!
cookie.add(1)
cookie.add(1)

cookie.add(2)
cookie.delete(1)
console.log(cookie.has(1))
console.log(cookie.size)

var weakset = new WeakSet()
var foo = {
  name: 'sunaiwen'
}
weakset.add(foo)
console.log(weakset.has(foo))