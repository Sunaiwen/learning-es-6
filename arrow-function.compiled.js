// The THIS inheritance
'use strict';

var foo = {
  bar: function bar() {
    var self = undefined;(function () {
      console.log(self === undefined);
    })();
  }
};

foo.bar();

// don't have arguments object passing in
(function (a) {
  for (var _len = arguments.length, b = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    b[_key - 1] = arguments[_key];
  }

  console.log(a);
  console.log(b);
})(1, 2, 3);

// shorthand for writing function in object
var bar = {
  test: function test() {
    console.log(this.name === 'bar');
  },
  name: 'bar'
};
bar.test();
