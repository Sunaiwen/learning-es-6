// array

var arr1 = [1, 2, 3, 4, 'haha', false]

for(var k1 of arr1) {
  console.log(k1)
}

var str1 = 'sunaiwen'

for(var k2 of str1) {
  console.log(k2)
}

var obj1 = {
  '0': 'zero',
  '1': 'one',
  '2': 'two',
  '3': 'three',
  length: 4
}
obj1[Symbol.iterator] = Array.prototype[Symbol.iterator]

for(var k3 of obj1) {
  console.log(k3)
}