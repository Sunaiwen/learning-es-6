// chain, then, catch
var promise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    reject('Haha')
  }, 1000)
})

promise
.then(console.log)
.catch(function(err) {
  console.error('Error:', err)
})

// sequence
var sequence = Promise.resolve()
;[1,2,3,4,5,6].forEach(function(index) {
  sequence = sequence.then(function() {
    console.log(index)
    var time = Math.floor(Math.random() * 100)
    console.log('time:' + time)
    return new Promise(function(res, rej) {
      setTimeout(function(){
        res(index)
      }, time)
    })
  })
})

//promise with generator
var gen = function*() {
  var sequence = Promise.resolve()
  var arr = []
  for(var i=0; i < 5; i++) {
    arr.push(new Promise(function(res, rej) {
      setTimeout(function() {
        res(i)
      }, 100)
    }))
  }

  for(i=0; i < arr.length; i++) {
    yield arr[i]
  }
}()

for(var g of gen) {
  g.then(function(val) {
    console.log('From generator:' + val)
  })
}